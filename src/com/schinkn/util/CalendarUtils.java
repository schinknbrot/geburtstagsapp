package com.schinkn.util;

import java.util.ArrayList;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;
import android.text.format.Time;
import android.util.Log;

import com.schinkn.MainActivity;
import com.schinkn.model.Person;

/**
 * Utility class to handle access to the Calendar
 * @author julius
 *
 */
public class CalendarUtils {

    private static final String DEBUG_TAG = "com.schinkn.util.CalenderUtils";

    public static final String MY_ACCOUNT_NAME = "geburtstags@kalender.com";
    public static final String CALENDER_MAIL = "geburtstags@kalender.com";
    public static final String CALENDAR_NAME = "Geburtstagskalender";

    public static final String NEW_PERSON_INTENT_KEY = "newly created Person";

    // Projection array. Creating indices for this array instead of doing
    // dynamic lookups improves performance.
    public static final String[] CALENDAR_FIELDS = new String[]{ Calendars._ID, // 0
                                                                Calendars.ACCOUNT_NAME, // 1
                                                                Calendars.CALENDAR_DISPLAY_NAME, // 2
                                                                Calendars.OWNER_ACCOUNT // 3
    };

    // The indices for the projection array above.
    private static final int CALENDAR_ID_INDEX = 0;

    private static final String[] EVENTS_PROJECTION = { Events._ID,
                                                       Events.TITLE,
                                                       Events.DTSTART,
                                                       Events.DURATION,
                                                       Events.DESCRIPTION,
                                                       Events.CALENDAR_ID,
                                                       Events.EVENT_LOCATION };
    // private static final int CALENDAR_OWNER_ACCOUNT_INDEX = 3;

    private final ContentResolver cr;
    private final MainActivity context;

    public CalendarUtils(MainActivity context) {

        this.context = context;
        cr = context.getContentResolver();

        getAllEvents();
    }

    public long getCalendarId() {

        // Run query
        Cursor cur = null;
        Uri uri = Calendars.CONTENT_URI;
        String selection = "((" + Calendars.ACCOUNT_NAME + " = ?) AND (" + Calendars.ACCOUNT_TYPE
                           + " = ?) AND (" + Calendars.OWNER_ACCOUNT + " = ?))";

        String[] selectionArgs = new String[]{ MY_ACCOUNT_NAME, "LOCAL", MY_ACCOUNT_NAME };
        // Submit the query and get a Cursor object back.
        cur = cr.query(uri, CALENDAR_FIELDS, selection, selectionArgs, null);
        // Use the cursor to step through the returned records
        long calID = -1;
        while(cur.moveToNext()) {
            // Get the field values
            calID = cur.getLong(CALENDAR_ID_INDEX);
        }
        cur.close();

        return calID;
    }

    public void getAllEvents() {

        Log.d(DEBUG_TAG, "Getting all Events:");
        Uri local_uri = Uri.parse("content://com.android.calendar/events");

        String selection = Events.CALENDAR_ID + " = 5";
        Cursor cursor = cr.query(local_uri, EVENTS_PROJECTION, selection, null, "_id");

        cursor.moveToFirst();

        while(cursor.moveToNext()) {

            Log.d(DEBUG_TAG, "CAL_ID : " + cursor.getString(cursor.getColumnIndex(Events.CALENDAR_ID))
                             + " _ EVENT_ID : " + cursor.getString(cursor.getColumnIndex(Events._ID))
                             + " _ TITLE : " + cursor.getString(cursor.getColumnIndex(Events.TITLE)));

        }

        Log.d(DEBUG_TAG, "Last EventID: " + getLastEventID());
    }

    public long getEventIdForPerson(
                                    Person person) {

        long eventID = Person.NEW_ID;
        Log.d(DEBUG_TAG, "Getting Eventid for: " + person.toDebug());
        Uri local_uri = Uri.parse("content://com.android.calendar/events");

        String selection = Events.EVENT_LOCATION + "=? and " + Events.CALENDAR_ID + "=5";
        String[] selectionArgs = { String.valueOf(person.get_id()) };

        Cursor cursor = cr.query(local_uri, EVENTS_PROJECTION, selection, selectionArgs, "_id");

        cursor.moveToFirst();

        if(cursor.moveToNext()) {
            eventID = cursor.getLong(cursor.getColumnIndex(Events._ID));
        }

        return eventID;
    }

    public long getLastEventID() {

        Uri local_uri = Uri.parse("content://com.android.calendar/events");

        Cursor cursor = cr.query(local_uri, new String[]{ "MAX(_id) as max_id" }, null, null, "_id");

        cursor.moveToFirst();
        long max_val = cursor.getLong(cursor.getColumnIndexOrThrow("max_id"));
        return max_val;

    }

    public long getNextEventID() {

        Uri local_uri = Uri.parse("content://com.android.calendar/events");

        Cursor cursor = cr.query(local_uri, new String[]{ "MAX(_id) as max_id" }, null, null, "_id");

        cursor.moveToFirst();
        long max_val = cursor.getLong(cursor.getColumnIndexOrThrow("max_id"));
        return max_val + 1;

    }

    public void putAllPersonsWithSyncToCalendar() {

        long calID = getCalendarId();

        if(calID == -1) {
            calID = createCalendar();
        }

        Log.d(DEBUG_TAG, "Using Calender: " + CALENDAR_NAME);

        ArrayList<Person> personen = MainActivity.getDatasource().getAllPersons();

        for(Person person : personen) {
            putPersonWithSyncToCalendar(calID, person);
        }
    }

    public void putPersonsWithSyncToCalendar(
                                             ArrayList<Person> personen) {

        long calID = getCalendarId();

        if(calID == -1) {
            calID = createCalendar();
        }

        Log.d(DEBUG_TAG, "Using Calender: " + CALENDAR_NAME);

        for(Person person : personen) {
            putPersonWithSyncToCalendar(calID, person);
        }
    }

    public void putPersonWithSyncToCalendar(
                                            long calID,
                                            Person person) {

        Log.d(DEBUG_TAG, "Using Calender: " + CALENDAR_NAME);

        if(person.isSyncWithCalendar()) {

            if(person.getEventId() == Person.NEW_ID) {
                insertPersonToCalendar(calID, person);
            } else {
                updatePersonInCalendar(person);
            }
        } else {
            if(person.getEventId() != Person.NEW_ID) {
                deletePersonFromCalendar(person);
            } else {
                Log.d(DEBUG_TAG, "Nothing to do");
            }
        }

    }

    private void updatePersonInCalendar(
                                        Person person) {

        getAllEvents();
        Log.d(DEBUG_TAG, "Update: " + person.toDebug());

        long eventID = person.getEventId();

        Uri updateUri = null;
        // The new title for the event

        updateUri = ContentUris.withAppendedId(Events.CONTENT_URI, eventID);
        cr.update(updateUri, person.getContentValues(), null, null);
    }

    public boolean deletePersonFromCalendar(
                                            Person person) {

        Log.d(DEBUG_TAG, "Delete: " + person.toDebug());
        String[] selArgs = new String[]{ Long.toString(person.getEventId()) };
        cr.delete(Events.CONTENT_URI, Events._ID + " =? ", selArgs);
        person.setEventId(Person.NEW_ID);
        MainActivity.getDatasource().createOrUpdatePerson(person);
        return true;
    }

    private void insertPersonToCalendar(
                                        long calID,
                                        Person person) {

        Log.d(DEBUG_TAG, "Insert: " + person.toDebug() + " into Calendar Nr " + calID);
        System.out.println("datasource: " + MainActivity.getDatasource());
        //        person.setEventId(getNextEventID());
        MainActivity.getDatasource().createOrUpdatePerson(person);

        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent = person.getIntentValues(intent, calID);

        context.startActivityForResult(intent, 999);

        // get the event ID that is the last element in the Uri
        //        long eventId = Long.parseLong(uri.getLastPathSegment());
        //        int eventId = -2;
        //        person.setEventId(eventId);
        //        datasource.createOrUpdatePerson(person);

    }

    private long createCalendar() {

        ContentValues values = new ContentValues();
        values.put(Calendars.ACCOUNT_NAME, MY_ACCOUNT_NAME);
        values.put(Calendars.ACCOUNT_TYPE, CalendarContract.ACCOUNT_TYPE_LOCAL);
        values.put(Calendars.NAME, CALENDAR_NAME);
        values.put(Calendars.CALENDAR_DISPLAY_NAME, CALENDAR_NAME);
        values.put(Calendars.CALENDAR_COLOR, 0xffff0000);
        values.put(Calendars.CALENDAR_ACCESS_LEVEL, Calendars.CAL_ACCESS_OWNER);
        values.put(Calendars.OWNER_ACCOUNT, CALENDER_MAIL);
        values.put(Calendars.CALENDAR_TIME_ZONE, Time.TIMEZONE_UTC);
        values.put(Calendars.SYNC_EVENTS, 1);
        Uri.Builder builder = CalendarContract.Calendars.CONTENT_URI.buildUpon();
        builder.appendQueryParameter(Calendars.ACCOUNT_NAME, MY_ACCOUNT_NAME);
        builder.appendQueryParameter(Calendars.ACCOUNT_TYPE, CalendarContract.ACCOUNT_TYPE_LOCAL);
        builder.appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "true");
        Uri uri = cr.insert(builder.build(), values);
        System.out.println(uri.toString());
        Log.d(DEBUG_TAG, "Created Calender at: " + uri.toString());

        return getCalendarId();
    }

    public ArrayList<Long> getAllEventIDs() {

        Log.d(DEBUG_TAG, "Getting all Eventids:");
        Uri local_uri = Uri.parse("content://com.android.calendar/events");
        ArrayList<Long> eventIDs = new ArrayList<Long>();

        String selection = Events.CALENDAR_ID + " = 5";
        Cursor cursor = cr.query(local_uri, EVENTS_PROJECTION, selection, null, "_id");

        cursor.moveToFirst();

        while(cursor.moveToNext()) {
            eventIDs.add(cursor.getLong(cursor.getColumnIndex(Events._ID)));
            Log.d(DEBUG_TAG, "ID: " + cursor.getLong(cursor.getColumnIndex(Events._ID)));
        }
        return eventIDs;
    }
}
