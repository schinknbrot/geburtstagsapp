package com.schinkn.db;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TreeMap;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.schinkn.MainActivity;
import com.schinkn.model.Person;
import com.schinkn.util.CalendarUtils;

public class PersonDataSource {

    // Database fields
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private CalendarUtils calUtil;

    private static final String DEBUG_TAG = PersonDataSource.class.getCanonicalName();

    public PersonDataSource(MainActivity context) {

        dbHelper = new MySQLiteHelper(context);
        calUtil = new CalendarUtils(context);
    }

    public void open() throws SQLException {

        database = dbHelper.getWritableDatabase();
        dbHelper.onCreate(database);
    }

    public void close() {

        dbHelper.close();
    }

    public Person getPersonWithID(
                                  long _id) {

        if(_id == Person.NEW_ID) {
            return createOrUpdatePerson(new Person());
        }
        Person person;
        Cursor cursor = database.query(Person.TABLE_PERSONS,
                                       Person.ALL_COLUMNS,
                                       Person.COLUMN_ID + " = " + _id,
                                       null,
                                       null,
                                       null,
                                       null);
        cursor.moveToFirst();
        person = cursorToPerson(cursor);
        cursor.close();
        return person;
    }

    public Person insertPerson(
                               Person person) {

        ContentValues values = new ContentValues();
        values.put(Person.COLUMN_DATE, person.getGeburtsdatum());
        values.put(Person.COLUMN_NAME, person.getName());
        values.put(Person.COLUMN_VORNAME, person.getPrename());
        values.put(Person.COLUMN_WISH, person.getPresentWish());
        values.put(Person.COLUMN_IMG, person.getImg());
        values.put(Person.COLUMN_SYNC, person.isSyncWithCalendar());
        values.put(Person.COLUMN_EVENT_ID, person.getEventId());

        int insertId = ( int ) database.insert(Person.TABLE_PERSONS, null, values);
        Cursor cursor = database.query(Person.TABLE_PERSONS,
                                       Person.ALL_COLUMNS,
                                       Person.COLUMN_ID + " = " + insertId,
                                       null,
                                       null,
                                       null,
                                       null);
        cursor.moveToFirst();
        person = cursorToPerson(cursor);
        person.setId(insertId);
        cursor.close();
        return person;
    }

    public Person updatePerson(
                               Person person) {

        ContentValues values = new ContentValues();
        values.put(Person.COLUMN_DATE, person.getGeburtsdatum());
        values.put(Person.COLUMN_NAME, person.getName());
        values.put(Person.COLUMN_VORNAME, person.getPrename());
        values.put(Person.COLUMN_WISH, person.getPresentWish());
        values.put(Person.COLUMN_IMG, person.getImg());
        values.put(Person.COLUMN_SYNC, person.isSyncWithCalendar());
        values.put(Person.COLUMN_EVENT_ID, person.getEventId());

        database.update(Person.TABLE_PERSONS, values, Person.COLUMN_ID + " = " + person.get_id(), null);
        return person;
    }

    public Person createOrUpdatePerson(
                                       Person person) {

        if(person.isEmpty()) {
            return person;
        }

        if(person.get_id() == Person.NEW_ID) {
            person = insertPerson(person);

            Log.d(DEBUG_TAG, "Insert: " + person.toDebug());
        } else {

            person = updatePerson(person);
            Log.d(DEBUG_TAG, "Update: " + person.toDebug());
        }

        return person;
    }

    public void deletePerson(
                             Person person) {

        Log.d(DEBUG_TAG, "Deleting " + person.toDebug() + " from DB");
        if(calUtil.deletePersonFromCalendar(person)) {

            long id = person.get_id();
            database.delete(Person.TABLE_PERSONS, Person.COLUMN_ID + " = " + id, null);
        }
    }

    public TreeMap<String, ArrayList<Person>> getPersonMap() {

        TreeMap<String, ArrayList<Person>> persons = new TreeMap<String, ArrayList<Person>>();

        Cursor cursor = database.query(Person.TABLE_PERSONS, Person.ALL_COLUMNS, null, null, null, null, null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Person person = cursorToPerson(cursor);
            String[] date = person.getGeburtsdatum().toString().split("-");
            String key = date[2] + "-" + date[1];
            if(!persons.containsKey(key)) {
                persons.put(key, new ArrayList<Person>());
            }
            persons.get(key).add(person);
            cursor.moveToNext();

            Collections.sort(persons.get(key));
        }
        // make sure to close the cursor
        cursor.close();
        return persons;
    }

    public ArrayList<Person> getAllPersons() {

        ArrayList<Person> persons = new ArrayList<Person>();

        Cursor cursor = database.query(Person.TABLE_PERSONS, Person.ALL_COLUMNS, null, null, null, null, null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Person person = cursorToPerson(cursor);
            persons.add(person);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return persons;
    }

    private Person cursorToPerson(
                                  Cursor cursor) {

        long id = cursor.getLong(0), eventId = cursor.getLong(7);
        String date = cursor.getString(1), vorname = cursor.getString(3), nachname = cursor.getString(2), wish = cursor.getString(4);

        byte[] img = cursor.getBlob(5);

        boolean sync = cursor.getInt(6) == 1;

        Person person = new Person(date, nachname, vorname, wish, img, sync);
        person.setId(id);
        person.setEventId(eventId);
        return person;
    }

    public ArrayList<String> getSortedDates() {

        ArrayList<String> results = new ArrayList<String>();

        Cursor cursor = database.rawQuery("SELECT DISTINCT " + Person.COLUMN_DATE + " FROM "
                                          + Person.TABLE_PERSONS + " ORDER BY " + " strftime('%m%d%Y',"
                                          + Person.COLUMN_DATE + ")", null);
        Log.d(DEBUG_TAG, "Load sorted dates");
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            String[] date = cursor.getString(0).split("-");
            Log.d(DEBUG_TAG, cursor.getString(0));
            results.add(date[2] + "-" + date[1]);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

        final int UNDEF_INDEX = -1;
        int indexOfNext = UNDEF_INDEX;

        Calendar cal = new GregorianCalendar();
        int todayDay = cal.get(Calendar.DAY_OF_MONTH);
        int todayMonth = cal.get(Calendar.MONTH) + 1;

        for(int i = 0; i < results.size(); i++) {
            String[] res = results.get(i).split("-");

            if(Integer.parseInt(res[1]) == todayMonth && Integer.parseInt(res[0]) == todayDay) {
                indexOfNext = i;
                break;
            } else if(Integer.parseInt(res[1]) == todayMonth && Integer.parseInt(res[0]) > todayDay) {
                indexOfNext = i;
                break;
            } else if(Integer.parseInt(res[1]) > todayMonth) {
                indexOfNext = i;
                break;
            }
        }

        if(indexOfNext != UNDEF_INDEX) {
            // rearrange birthdays
            String[] resultArray = results.toArray(new String[results.size()]);

            results.clear();

            for(; indexOfNext < resultArray.length; indexOfNext++) {
                results.add(resultArray[indexOfNext]);
                if(indexOfNext == resultArray.length - 1) {
                    break;
                }

            }

            for(int i = 0; i < indexOfNext; i++) {
                results.add(resultArray[i]);

            }

        }

        return results;
    }

    public void debug() {

        Log.d(DEBUG_TAG, "Print all Persons from DB");
        Cursor cursor = database.query(Person.TABLE_PERSONS, Person.ALL_COLUMNS, null, null, null, null, null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Log.d(DEBUG_TAG, new Date().toString() + " - " + cursorToPerson(cursor).toDebug());
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
    }

    public ArrayList<Long> getAllEventIDs() {

        ArrayList<Person> persons = getAllPersons();
        ArrayList<Long> eventids = new ArrayList<Long>();
        for(Person person : persons) {
            if(person.getEventId() != Person.NEW_ID) {
                eventids.add(person.getEventId());
            }
        }
        return eventids;
    }

}