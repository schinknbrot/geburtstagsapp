package com.schinkn.db;

import java.util.ArrayList;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.schinkn.MainActivity;
import com.schinkn.model.Person;

public class MySQLiteHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "persons.db";
	private static final int DATABASE_VERSION = 1;

	// Database creation sql statement
	private static final String DATABASE_CREATE = Person.CREATE_TABLE;

	public MySQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		// database.execSQL("DROP TABLE IF EXISTS " + Person.TABLE_PERSONS);
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
		Log.w(MySQLiteHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", any data stored in the old database will be transferred.");
		PersonDataSource datasource = MainActivity.getDatasource();
		
		ArrayList<Person> personList = datasource.getAllPersons();
		database.execSQL("DROP TABLE IF EXISTS " + Person.TABLE_PERSONS);
		onCreate(database);
		
		for (Person person : personList) {
			datasource.createOrUpdatePerson(person);
		}
	}

}