package com.schinkn;

import java.util.ArrayList;
import java.util.TreeMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import com.schinkn.db.PersonDataSource;
import com.schinkn.model.Person;
import com.schinkn.util.CalendarUtils;

/**
 * This Class represents the MainActivity of the BirthdayListApp.<br>
 * 
 * It extends Activity and implements OnItemClickListener and OnClickListener.
 * 
 * 
 * @author Julius Breckel [546092] 
 *
 */
public class MainActivity extends Activity implements OnItemClickListener, OnClickListener {

    /**
     * Adapter for ListView Content
     */
    private PersonListAdapter adapter;

    /**
     * ListView for persons
     */
    private ListView personListView;

    /**
     * DataSource to access the persons DB
     */
    private static PersonDataSource dataSource;

    private static final String DEBUG_TAG = "com.schinkn.MainActivity";

    public static final String CLICKED_PERSON = DEBUG_TAG + ".clickedPerson";

    private CalendarUtils calUtils;

    @Override
    public void onCreate(
                         Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "MainActivity.onCreate");
        calUtils = new CalendarUtils(this);
        /*
         * PersonDataSource is opened. Database is also created if not exists
         * already
         */
        dataSource = new PersonDataSource(this);
        dataSource.open();

        System.out.println(dataSource);

        /*
         * Loads the main layout as ContentView
         */
        setContentView(R.layout.main_layout);

        initLayout();

        calUtils.getAllEvents();
    }

    @Override
    public boolean onCreateOptionsMenu(
                                       Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {

        super.onResume();
        loadFromDB();
    }

    @Override
    protected void onActivityResult(
                                    int requestCode,
                                    int resultCode,
                                    Intent data) {

        Log.d(DEBUG_TAG + ".onActivityResult", data + " - " + resultCode);

        Toast.makeText(this, data + " - " + resultCode, Toast.LENGTH_SHORT).show();

        verifyEventsAndPersons();

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void verifyEventsAndPersons() {

        Log.d(DEBUG_TAG + ".verifyEvents", "Verifying if Persons have existing Events tagged to them");

        //        calUtils.getAllEvents(); 

        ArrayList<Person> persons = dataSource.getAllPersons();

        for(Person person : persons) {
            Log.d(DEBUG_TAG + ".verifyEvents", "" + person.getEventId());
            Log.d(DEBUG_TAG, "getting eventID of Person" + calUtils.getEventIdForPerson(person));

            person.setEventId(calUtils.getEventIdForPerson(person));
            dataSource.createOrUpdatePerson(person);

            Log.d(DEBUG_TAG + ".verifyEvents", "Updated: " + person.getEventId());
        }
    }

    /**
     * Layout for {@link MainActivity} is initialized
     */
    private void initLayout() {

        /*
         * Create the ListView Adapter
         */
        adapter = new PersonListAdapter(this);

        /*
         * Get a reference to the ListView holder
         */
        personListView = ( ListView ) this.findViewById(R.id.list_persons);

        /*
         * Listen for Click events
         */
        personListView.setOnItemClickListener(this);
        personListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        personListView.setMultiChoiceModeListener(new ModeCallback());

    }

    /**
     * Loads persons data from DB to ListView
     */
    private void loadFromDB() {

        TreeMap<String, ArrayList<Person>> personenMap = new TreeMap<String, ArrayList<Person>>();
        personenMap = dataSource.getPersonMap();
        ArrayList<String> dates = dataSource.getSortedDates();

        // If no PersonData is in DB than the empty ViewStub is opened
        if(dates.size() == 0) {
            if(findViewById(R.id.empty_view) != null) {
                ( ( ViewStub ) findViewById(R.id.empty_view) ).setVisibility(View.VISIBLE);
            }
        }

        // Iterate through all Dates from DB
        for(String date : dates) {
            ArrayList<Person> results = new ArrayList<Person>();

            Log.d(DEBUG_TAG, "Get List for Date: " + date);

            // Iterate through all Persons from this date and add them to the
            // result List
            for(Person key : personenMap.get(date)) {
                Log.d(DEBUG_TAG, "Person " + key.toDebug());
                results.add(key);
            }

            // if there are Results for the date
            if(results.size() > 0) {
                /*
                 * add a new section the the adapter with date as Header and a
                 * new ArrayAdapter from the result List
                 */
                adapter.addSection(date,
                                   new ArrayAdapter<Person>(this,
                                                            R.layout.list_item_person,
                                                            results.toArray(new Person[results.size()])));
            }
        }

        personListView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    /**
     * If on a ListViewItem is clicked, the {@link PersonShowActivity} is opened
     * with an Intent which has the _id of the {@link Person} to present all
     * data from that {@link Person} in the next Activity.
     */
    @Override
    public void onItemClick(
                            AdapterView<?> parent,
                            View view,
                            int position,
                            long id) {

        Person person = ( ( Person ) adapter.getItem(position) );

        Log.d(DEBUG_TAG + ".onItemClick", "Person: " + person.toDebug());

        /*
         * New Intent is created to start PersonShowActivity with the _id of the
         * clicked Person
         */
        Intent intent = new Intent(this, PersonShowActivity.class);
        intent.putExtra(CLICKED_PERSON, person.get_id());
        startActivity(intent);
    }

    /**
     * Method that is called when either Add Person, Sync Button or the CheckBox
     * belonging to a Person is clicked.
     */
    @Override
    public void onClick(
                        View v) {

        Intent intent = null;
        if(v.getTag() instanceof Person) {

            Log.d(DEBUG_TAG + ".onClick", ( ( Person ) v.getTag() ).toDebug());
        } else {

            Log.d(DEBUG_TAG + ".onClick", v.getTag().toString());
        }

        switch(v.getId()){
        //Button to create a new Person is clicked
            case R.id.button_add_person:
                intent = new Intent(this, PersonShowActivity.class);
                intent.putExtra(CLICKED_PERSON, Person.NEW_ID);
                startActivity(intent);
                break;
            case R.id.checkbox_sync:
                CheckBox cb = ( CheckBox ) v.findViewById(R.id.checkbox_sync);

                Person person = ( Person ) cb.getTag();

                person.setSyncWithCalendar(cb.isChecked());
                dataSource.createOrUpdatePerson(person);
                break;
        }
    }

    /**
     * Gives access to the PersonDataSource used by the App 
     * @return dataSource
     */
    public static PersonDataSource getDatasource() {

        return dataSource;
    }

    private class ModeCallback implements ListView.MultiChoiceModeListener {

        @Override
        public boolean onCreateActionMode(
                                          ActionMode mode,
                                          Menu menu) {

            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.list_select_menu, menu);
            mode.setTitle("Sync selected Items");
            return true;
        }

        @Override
        public boolean onPrepareActionMode(
                                           ActionMode mode,
                                           Menu menu) {

            return true;
        }

        @Override
        public boolean onActionItemClicked(
                                           ActionMode mode,
                                           MenuItem item) {

            Toast.makeText(MainActivity.this, "Clicked " + item.getTitle(), Toast.LENGTH_SHORT).show();
            switch(item.getItemId()){
                default:

                    mode.finish();
                    break;
            }
            return true;
        }

        @Override
        public void onDestroyActionMode(
                                        ActionMode mode) {

            PersonListAdapter pAdapter = ( PersonListAdapter ) personListView.getAdapter();
            ArrayList<Person> pList = new ArrayList<Person>();

            for(int i : checkedPositions) {
                Log.d(DEBUG_TAG, "Add Person: " + ( ( Person ) pAdapter.getItem(i) ).toDebug()
                                 + " to List of checked");
                pList.add(( Person ) pAdapter.getItem(i));
            }

            checkedPositions.removeAll(checkedPositions);

            new CalendarUtils(MainActivity.this).putPersonsWithSyncToCalendar(pList);

        }

        private ArrayList<Integer> checkedPositions = new ArrayList<Integer>();

        @Override
        public void onItemCheckedStateChanged(
                                              ActionMode mode,
                                              int position,
                                              long id,
                                              boolean checked) {

            if(checked) {
                checkedPositions.add(position);
            } else {
                checkedPositions.remove(Integer.valueOf(position));
            }

            final int checkedCount = personListView.getCheckedItemCount();
            switch(checkedCount){
                case 0:
                    mode.setSubtitle(null);
                    break;
                case 1:
                    mode.setSubtitle("One item selected");
                    break;
                default:
                    mode.setSubtitle("" + checkedCount + " items selected");
                    break;
            }

        }

    }
}