package com.schinkn;

import java.util.LinkedHashMap;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.schinkn.model.Person;
import com.schinkn.util.BitmapUtility;

public class PersonListAdapter extends BaseAdapter {
    private final Map<String, Adapter> sections = new LinkedHashMap<String, Adapter>();
    private final ArrayAdapter<String> headers;

    private final LayoutInflater inflater;

    public final static int TYPE_SECTION_HEADER = 0;

    public static final String[] monate = { null,
                                           "Januar",
                                           "Februar",
                                           "M�rz",
                                           "April",
                                           "Mai",
                                           "Juni",
                                           "Juli",
                                           "August",
                                           "September",
                                           "Oktober",
                                           "November",
                                           "Dezember" };

    public static final String[] monateShort = { null,
                                                "Jan",
                                                "Feb",
                                                "Mar",
                                                "Apr",
                                                "Mai",
                                                "Jun",
                                                "Jul",
                                                "Aug",
                                                "Sep",
                                                "Okt",
                                                "Nov",
                                                "Dez" };

    public PersonListAdapter(MainActivity parent) {

        headers = new ArrayAdapter<String>(parent, R.layout.list_header);

        inflater = ( LayoutInflater ) parent.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addSection(
                           String section,
                           Adapter adapter) {

        this.headers.add(section);
        this.sections.put(section, adapter);
    }

    @Override
    public Object getItem(
                          int position) {

        for(Object section : this.sections.keySet()) {
            Adapter adapter = sections.get(section);
            int size = adapter.getCount() + 1;

            // check if position inside this section
            if(position == 0) {
                return section;
            }
            if(position < size) {
                return adapter.getItem(position - 1);
            }

            // otherwise jump into next section
            position -= size;
        }
        return null;
    }

    @Override
    public int getCount() {

        // total together all sections, plus one for each section header
        int total = 0;
        for(Adapter adapter : this.sections.values())
            total += adapter.getCount() + 1;
        return total;
    }

    @Override
    public int getViewTypeCount() {

        // assume that headers count as one, then total all sections
        int total = 1;
        for(Adapter adapter : this.sections.values()) {
            total += adapter.getViewTypeCount();
        }
        return total;
    }

    @Override
    public int getItemViewType(
                               int position) {

        int type = 1;
        for(Object section : this.sections.keySet()) {
            Adapter adapter = sections.get(section);
            int size = adapter.getCount() + 1;

            // check if position inside this section
            if(position == 0)
                return TYPE_SECTION_HEADER;
            if(position < size)
                return type + adapter.getItemViewType(position - 1);

            // otherwise jump into next section
            position -= size;
            type += adapter.getViewTypeCount();
        }
        return -1;
    }

    public boolean areAllItemsSelectable() {

        return false;
    }

    @Override
    public boolean isEnabled(
                             int position) {

        return ( getItemViewType(position) != TYPE_SECTION_HEADER );
    }

    @SuppressWarnings("null")
    @Override
    public View getView(
                        int position,
                        View convertView,
                        ViewGroup parent) {

        ViewHolder holder = null;
        int sectionnum = 0;
        for(Object section : this.sections.keySet()) {
            Adapter adapter = sections.get(section);
            int size = adapter.getCount() + 1;

            // check if position inside this section
            if(position == 0) {

                TextView header = ( TextView ) headers.getView(sectionnum, convertView, parent);
                String[] headerText = ( ( String ) header.getText() ).split("-");
                header.setText(headerText[0] + ". "
                               + monate[Integer.parseInt( ( ( String ) header.getText() ).split("-")[1])]);
                return header;
            }
            if(position < size) {

                if(convertView == null) {
                    convertView = inflater.inflate(R.layout.list_item_person, parent, false);

                    holder = new ViewHolder();

                    holder.name = ( TextView ) convertView.findViewById(R.id.show_textview_name_full);
                    holder.wunsch = ( TextView ) convertView.findViewById(R.id.show_textview_wunsch);
                    holder.age = ( TextView ) convertView.findViewById(R.id.show_textview_age);
                    holder.thumbImage = ( ImageView ) convertView.findViewById(R.id.show_image_icon);
                    holder.syncBox = ( CheckBox ) convertView.findViewById(R.id.checkbox_sync);

                    convertView.setTag(holder);

                } else {
                    holder = ( ViewHolder ) convertView.getTag();
                }

                final Person p = ( ( Person ) adapter.getItem(position - 1) );

                holder.person = p;
                holder.name.setText(p.getFullName());
                holder.wunsch.setText(p.getPresentWish());
                holder.age.setText("" + p.getNextAge());
                holder.thumbImage.setImageBitmap(BitmapUtility.getPhoto(p.getImg()));

                holder.syncBox.setChecked(p.isSyncWithCalendar());
                holder.syncBox.setTag(p);

                return convertView;
            }

            // otherwise jump into next section
            position -= size;
            sectionnum++;
        }
        return null;
    }

    static class ViewHolder {
        Person person;
        TextView name;
        TextView wunsch;
        TextView age;
        ImageView thumbImage;
        CheckBox syncBox;
    }

    @Override
    public long getItemId(
                          int position) {

        return position;
    }

}
