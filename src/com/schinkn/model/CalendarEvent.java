package com.schinkn.model;

import java.util.Calendar;
import java.util.TimeZone;

import android.content.ContentValues;
import android.provider.CalendarContract.Events;

public class CalendarEvent {

	private long startTime;
	private String title;
	private String rrule;
	private String description;
	private String timeZone;
	private boolean allDay;

	public CalendarEvent(long startTime, String title, String rrule, String description, String timeZone, boolean allDay) {

		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		cal.setTimeInMillis(startTime);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);

		System.out.println(cal.getTime());

		this.startTime = cal.getTimeInMillis();
		this.title = title;
		this.rrule = rrule;
		this.description = description;
		this.timeZone = timeZone;
		this.allDay = allDay;
	}

	public ContentValues getValues(long calID) {

		ContentValues values = new ContentValues();

		values.put(Events.DTSTART, startTime);
		values.put(Events.DURATION, "P0D");
		values.put(Events.TITLE, title);
		values.put(Events.RRULE, rrule);
		values.put(Events.DESCRIPTION, description);
		values.put(Events.CALENDAR_ID, calID);
		values.put(Events.EVENT_TIMEZONE, timeZone);
		values.put(Events.ALL_DAY, allDay ? 1 : 0);
		return values;
	}

	public ContentValues getValues() {

		ContentValues values = new ContentValues();

		values.put(Events.DTSTART, startTime);
		values.put(Events.DURATION, "P0D");
		values.put(Events.TITLE, title);
		values.put(Events.RRULE, rrule);
		values.put(Events.DESCRIPTION, description);
		values.put(Events.EVENT_TIMEZONE, timeZone);
		values.put(Events.ALL_DAY, allDay ? 1 : 0);
		return values;
	}

	public long getStartTime() {

		return startTime;
	}

	public void setStartTime(long startTime) {

		this.startTime = startTime;
	}

	public String getTitle() {

		return title;
	}

	public void setTitle(String title) {

		this.title = title;
	}

	public String getRrule() {

		return rrule;
	}

	public void setRrule(String rrule) {

		this.rrule = rrule;
	}

	public String getDescription() {

		return description;
	}

	public void setDescription(String description) {

		this.description = description;
	}

	public boolean isAllDay() {

		return allDay;
	}

	public void setAllDay(boolean allDay) {

		this.allDay = allDay;
	}

	public String getTimeZone() {

		return timeZone;
	}

	public void setTimeZone(String timeZone) {

		this.timeZone = timeZone;
	}

}
